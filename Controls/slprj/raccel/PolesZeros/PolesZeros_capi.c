#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "PolesZeros_capi_host.h"
#define sizeof(s) ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el) ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s) (s)
#ifndef SS_UINT64
#define SS_UINT64 17
#endif
#ifndef SS_INT64
#define SS_INT64 18
#endif
#else
#include "builtin_typeid_types.h"
#include "PolesZeros.h"
#include "PolesZeros_capi.h"
#include "PolesZeros_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST
#define TARGET_STRING(s)               ((NULL))
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif
static const rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 0 , TARGET_STRING (
"PolesZeros/Step signal2" ) , TARGET_STRING ( "Input u(t)" ) , 0 , 0 , 0 , 0
, 0 } , { 1 , 0 , TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 1 } , { 0 , 0 , ( NULL ) , ( NULL ) ,
0 , 0 , 0 , 0 , 0 } } ; static const rtwCAPI_BlockParameters
rtBlockParameters [ ] = { { 2 , TARGET_STRING ( "PolesZeros/Step signal2" ) ,
TARGET_STRING ( "Time" ) , 0 , 0 , 0 } , { 3 , TARGET_STRING (
"PolesZeros/Step signal2" ) , TARGET_STRING ( "Before" ) , 0 , 0 , 0 } , { 4
, TARGET_STRING ( "PolesZeros/Step signal2" ) , TARGET_STRING ( "After" ) , 0
, 0 , 0 } , { 5 , TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) ,
TARGET_STRING ( "A_pr" ) , 0 , 1 , 0 } , { 6 , TARGET_STRING (
"PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "A_ir" ) , 1 , 1 , 0 } , { 7
, TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "A_jc" ) ,
1 , 2 , 0 } , { 8 , TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) ,
TARGET_STRING ( "B_pr" ) , 0 , 0 , 0 } , { 9 , TARGET_STRING (
"PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "B_ir" ) , 1 , 0 , 0 } , { 10
, TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "B_jc" ) ,
1 , 1 , 0 } , { 11 , TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) ,
TARGET_STRING ( "C_pr" ) , 0 , 0 , 0 } , { 12 , TARGET_STRING (
"PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "C_ir" ) , 1 , 0 , 0 } , { 13
, TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) , TARGET_STRING ( "C_jc" ) ,
1 , 2 , 0 } , { 14 , TARGET_STRING ( "PolesZeros/P(s)2/Internal" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 0 , ( NULL ) , ( NULL
) , 0 , 0 , 0 } } ; static int_T rt_LoggedStateIdxList [ ] = { - 1 } ; static
const rtwCAPI_Signals rtRootInputs [ ] = { { 0 , 0 , ( NULL ) , ( NULL ) , 0
, 0 , 0 , 0 , 0 } } ; static const rtwCAPI_Signals rtRootOutputs [ ] = { { 0
, 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 , 0 , 0 } } ; static const
rtwCAPI_ModelParameters rtModelParameters [ ] = { { 0 , ( NULL ) , 0 , 0 , 0
} } ;
#ifndef HOST_CAPI_BUILD
static void * rtDataAddrMap [ ] = { & rtB . ol2ah3yc1q , & rtB . mi3izpk2u3 ,
& rtP . Stepsignal2_Time , & rtP . Stepsignal2_Y0 , & rtP .
Stepsignal2_YFinal , & rtP . Internal_A_pr [ 0 ] , & rtP . Internal_A_ir [ 0
] , & rtP . Internal_A_jc [ 0 ] , & rtP . Internal_B_pr , & rtP .
Internal_B_ir , & rtP . Internal_B_jc [ 0 ] , & rtP . Internal_C_pr , & rtP .
Internal_C_ir , & rtP . Internal_C_jc [ 0 ] , & rtP .
Internal_InitialCondition , } ; static int32_T * rtVarDimsAddrMap [ ] = { (
NULL ) } ;
#endif
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "double" ,
"real_T" , 0 , 0 , sizeof ( real_T ) , ( uint8_T ) SS_DOUBLE , 0 , 0 , 0 } ,
{ "unsigned int" , "uint32_T" , 0 , 0 , sizeof ( uint32_T ) , ( uint8_T )
SS_UINT32 , 0 , 0 , 0 } } ;
#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif
static TARGET_CONST rtwCAPI_ElementMap rtElementMap [ ] = { { ( NULL ) , 0 ,
0 , 0 , 0 } , } ; static const rtwCAPI_DimensionMap rtDimensionMap [ ] = { {
rtwCAPI_SCALAR , 0 , 2 , 0 } , { rtwCAPI_VECTOR , 2 , 2 , 0 } , {
rtwCAPI_VECTOR , 4 , 2 , 0 } } ; static const uint_T rtDimensionArray [ ] = {
1 , 1 , 2 , 1 , 3 , 1 } ; static const real_T rtcapiStoredFloats [ ] = { 0.0
, 1.0 } ; static const rtwCAPI_FixPtMap rtFixPtMap [ ] = { { ( NULL ) , (
NULL ) , rtwCAPI_FIX_RESERVED , 0 , 0 , ( boolean_T ) 0 } , } ; static const
rtwCAPI_SampleTimeMap rtSampleTimeMap [ ] = { { ( const void * ) &
rtcapiStoredFloats [ 0 ] , ( const void * ) & rtcapiStoredFloats [ 1 ] , (
int8_T ) 1 , ( uint8_T ) 0 } , { ( const void * ) & rtcapiStoredFloats [ 0 ]
, ( const void * ) & rtcapiStoredFloats [ 0 ] , ( int8_T ) 0 , ( uint8_T ) 0
} } ; static rtwCAPI_ModelMappingStaticInfo mmiStatic = { { rtBlockSignals ,
2 , rtRootInputs , 0 , rtRootOutputs , 0 } , { rtBlockParameters , 13 ,
rtModelParameters , 0 } , { ( NULL ) , 0 } , { rtDataTypeMap , rtDimensionMap
, rtFixPtMap , rtElementMap , rtSampleTimeMap , rtDimensionArray } , "float"
, { 2932392216U , 4187496730U , 4039593947U , 2171463988U } , ( NULL ) , 0 ,
( boolean_T ) 0 , rt_LoggedStateIdxList } ; const
rtwCAPI_ModelMappingStaticInfo * PolesZeros_GetCAPIStaticMap ( void ) {
return & mmiStatic ; }
#ifndef HOST_CAPI_BUILD
void PolesZeros_InitializeDataMapInfo ( void ) { rtwCAPI_SetVersion ( ( *
rt_dataMapInfoPtr ) . mmi , 1 ) ; rtwCAPI_SetStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , & mmiStatic ) ; rtwCAPI_SetLoggingStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ; rtwCAPI_SetDataAddressMap ( ( *
rt_dataMapInfoPtr ) . mmi , rtDataAddrMap ) ; rtwCAPI_SetVarDimsAddressMap (
( * rt_dataMapInfoPtr ) . mmi , rtVarDimsAddrMap ) ;
rtwCAPI_SetInstanceLoggingInfo ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArray ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( ( * rt_dataMapInfoPtr ) . mmi , 0 ) ; }
#else
#ifdef __cplusplus
extern "C" {
#endif
void PolesZeros_host_InitializeDataMapInfo ( PolesZeros_host_DataMapInfo_T *
dataMap , const char * path ) { rtwCAPI_SetVersion ( dataMap -> mmi , 1 ) ;
rtwCAPI_SetStaticMap ( dataMap -> mmi , & mmiStatic ) ;
rtwCAPI_SetDataAddressMap ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetVarDimsAddressMap ( dataMap -> mmi , ( NULL ) ) ; rtwCAPI_SetPath
( dataMap -> mmi , path ) ; rtwCAPI_SetFullPath ( dataMap -> mmi , ( NULL ) )
; rtwCAPI_SetChildMMIArray ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( dataMap -> mmi , 0 ) ; }
#ifdef __cplusplus
}
#endif
#endif
