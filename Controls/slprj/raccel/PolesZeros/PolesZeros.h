#ifndef RTW_HEADER_PolesZeros_h_
#define RTW_HEADER_PolesZeros_h_
#ifndef PolesZeros_COMMON_INCLUDES_
#define PolesZeros_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "sigstream_rtw.h"
#include "simtarget/slSimTgtSigstreamRTW.h"
#include "simtarget/slSimTgtSlioCoreRTW.h"
#include "simtarget/slSimTgtSlioClientsRTW.h"
#include "simtarget/slSimTgtSlioSdiRTW.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "raccel.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "rt_logging_simtarget.h"
#include "dt_info.h"
#include "ext_work.h"
#endif
#include "PolesZeros_types.h"
#include <stddef.h>
#include "rtw_modelmap_simtarget.h"
#include "rt_defines.h"
#include <string.h>
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#define MODEL_NAME PolesZeros
#define NSAMPLE_TIMES (2) 
#define NINPUTS (0)       
#define NOUTPUTS (0)     
#define NBLOCKIO (2) 
#define NUM_ZC_EVENTS (0) 
#ifndef NCSTATES
#define NCSTATES (2)   
#elif NCSTATES != 2
#error Invalid specification of NCSTATES defined in compiler command
#endif
#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm) (*rt_dataMapInfoPtr)
#endif
#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val) (rt_dataMapInfoPtr = &val)
#endif
#ifndef IN_RACCEL_MAIN
#endif
typedef struct { real_T ol2ah3yc1q ; real_T mi3izpk2u3 ; } B ; typedef struct
{ struct { void * LoggedData [ 2 ] ; } ko2q1mq1zw ; int_T m5ecri1pdo ; } DW ;
typedef struct { real_T c4ppawjpka [ 2 ] ; } X ; typedef struct { real_T
c4ppawjpka [ 2 ] ; } XDot ; typedef struct { boolean_T c4ppawjpka [ 2 ] ; }
XDis ; typedef struct { real_T c4ppawjpka [ 2 ] ; } CStateAbsTol ; typedef
struct { real_T c4ppawjpka [ 2 ] ; } CXPtMin ; typedef struct { real_T
c4ppawjpka [ 2 ] ; } CXPtMax ; typedef struct { real_T e5hv4qrkys ; } ZCV ;
typedef struct { rtwCAPI_ModelMappingInfo mmi ; } DataMapInfo ; struct P_ {
real_T Stepsignal2_Time ; real_T Stepsignal2_Y0 ; real_T Stepsignal2_YFinal ;
real_T Internal_A_pr [ 2 ] ; real_T Internal_B_pr ; real_T Internal_C_pr ;
real_T Internal_InitialCondition ; uint32_T Internal_A_ir [ 2 ] ; uint32_T
Internal_A_jc [ 3 ] ; uint32_T Internal_B_ir ; uint32_T Internal_B_jc [ 2 ] ;
uint32_T Internal_C_ir ; uint32_T Internal_C_jc [ 3 ] ; } ; extern const char
* RT_MEMORY_ALLOCATION_ERROR ; extern B rtB ; extern X rtX ; extern DW rtDW ;
extern P rtP ; extern mxArray * mr_PolesZeros_GetDWork ( ) ; extern void
mr_PolesZeros_SetDWork ( const mxArray * ssDW ) ; extern mxArray *
mr_PolesZeros_GetSimStateDisallowedBlocks ( ) ; extern const
rtwCAPI_ModelMappingStaticInfo * PolesZeros_GetCAPIStaticMap ( void ) ;
extern SimStruct * const rtS ; extern const int_T gblNumToFiles ; extern
const int_T gblNumFrFiles ; extern const int_T gblNumFrWksBlocks ; extern
rtInportTUtable * gblInportTUtables ; extern const char * gblInportFileName ;
extern const int_T gblNumRootInportBlks ; extern const int_T
gblNumModelInputs ; extern const int_T gblInportDataTypeIdx [ ] ; extern
const int_T gblInportDims [ ] ; extern const int_T gblInportComplex [ ] ;
extern const int_T gblInportInterpoFlag [ ] ; extern const int_T
gblInportContinuous [ ] ; extern const int_T gblParameterTuningTid ; extern
DataMapInfo * rt_dataMapInfoPtr ; extern rtwCAPI_ModelMappingInfo *
rt_modelMapInfoPtr ; void MdlOutputs ( int_T tid ) ; void
MdlOutputsParameterSampleTime ( int_T tid ) ; void MdlUpdate ( int_T tid ) ;
void MdlTerminate ( void ) ; void MdlInitializeSizes ( void ) ; void
MdlInitializeSampleTimes ( void ) ; SimStruct * raccel_register_model (
ssExecutionInfo * executionInfo ) ;
#endif
