load LAMA3.mat;

Data_1 = table2array(HelicopterDataLAMA1);

time    = Data_1(:,1);
input_V  = Data_1(:,2);
output_V = Data_1(:,3);
output_m = Data_1(:,4);

initial_time = time(1,1);
time = time-initial_time;

initial_V = 2.5;
input_V = input_V - initial_V;

% this was autogenerated from curve fitter hehehaha

%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( time, output_m );
ft = 'linearinterp';
[fitresult, gof] = fit( xData, yData, ft, 'Normalize', 'on' );

fitted_output_m = fitresult;
[fitted_speed,fitted_accel] = differentiate(fitted_output_m,time);




subplot(3,1,1)
plot(time,input_V);
legend('input V')

subplot(3,1,2)
plot(time,output_m)
grid on
legend('output m')

subplot(3,1,3)
plot(time,fitted_speed)
grid on
hold on


final_value = fitted_speed(length(fitted_speed));
T_value = 0.632 * final_value;
T = time(find( abs(fitted_speed-T_value) == min(abs(fitted_speed-T_value))  ));

plot(T , T_value ,"r*")
legend('1st derivative - speed' , "63.2% point")


B = input_V(length(input_V));

A = final_value / B
T
S = output_m(:) ./ output_V(:);
S = mean(S(2:15));
S = S^(-1)


